import glob
import numpy as np
import matplotlib.pyplot as plt
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array, array_to_img
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout, InputLayer
from keras.models import Sequential
from keras import optimizers
from keras.applications import resnet
from keras.models import Model
from keras.models import load_model
import keras
import pandas as pd
from sklearn.preprocessing import LabelEncoder

IMG_DIM = (150, 150)

train_files = glob.glob('training_data2/*')
train_imgs = [img_to_array(image.load_img(img, target_size=IMG_DIM)) for img in train_files]
train_imgs = np.array(train_imgs)
train_labels = [fn.split('\\')[1].split('.')[0].strip() for fn in train_files]

validation_files = glob.glob('validation_data/*')
validation_imgs = [img_to_array(image.load_img(img, target_size=IMG_DIM)) for img in validation_files]
validation_imgs = np.array(validation_imgs)
validation_labels = [fn.split('\\')[1].split('.')[0].strip() for fn in validation_files]

print('Train dataset shape:', train_imgs.shape, 
      '\tValidation dataset shape:', validation_imgs.shape)
      
train_imgs_scaled = train_imgs.astype('float32')
validation_imgs_scaled  = validation_imgs.astype('float32')
train_imgs_scaled /= 255
validation_imgs_scaled /= 255

print(train_imgs[0].shape)
array_to_img(train_imgs[0])

batch_size = 30
num_classes = 2
epochs = 30
input_shape = (150, 150, 3)

# encode text category labels


le = LabelEncoder()
le.fit(train_labels)
train_labels_enc = le.transform(train_labels)
validation_labels_enc = le.transform(validation_labels)

print(train_labels[1495:1505], train_labels_enc[1495:1505])

res = resnet.ResNet50(include_top=False, weights='imagenet',input_shape=input_shape)
res_ram = resnet.ResNet50(include_top=False, weights=None,input_shape=input_shape)

output = res.layers[-1].output
output = keras.layers.Flatten()(output)
res_model = Model(res.input, output)

output_r = res_ram.layers[-1].output
output_r = keras.layers.Flatten()(output_r)
res_model_r = Model(res_ram.input, output_r)

for n,layer in enumerate(res_model.layers):
    if n>0:
        layer.set_weights(res_model_r.layers[n].get_weights())

res_model.trainable = True
for n,layer in enumerate(res_model.layers):
    layer.trainable = True
    #print(n)
    

pd.set_option('max_colwidth', -1)
layers = [(layer, layer.name, layer.trainable) for layer in res_model.layers]
pd.DataFrame(layers, columns=['Layer Type', 'Layer Name', 'Layer Trainable'])

def get_bottleneck_features(model, input_imgs):
    features = model.predict(input_imgs, verbose=0)
    return features
    
# train_features_vgg = get_bottleneck_features(vgg_model, train_imgs_scaled)
# validation_features_vgg = get_bottleneck_features(vgg_model, validation_imgs_scaled)

# print('Train Bottleneck Features:', train_features_vgg.shape, 
      # '\tValidation Bottleneck Features:', validation_features_vgg.shape)
      
      
train_datagen = ImageDataGenerator(rescale=1./255, zoom_range=0.3, rotation_range=50,
                                   width_shift_range=0.2, height_shift_range=0.2, shear_range=0.2, 
                                   horizontal_flip=True, fill_mode='nearest')

val_datagen = ImageDataGenerator(rescale=1./255)
train_generator = train_datagen.flow(train_imgs, train_labels_enc, batch_size=30)
val_generator = val_datagen.flow(validation_imgs, validation_labels_enc, batch_size=20)

input_shape = res_model.output_shape[1]

model = Sequential()
model.add(res_model)
model.add(Dense(512, activation='relu', input_dim=input_shape))
model.add(Dropout(0.3))
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.3))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer=optimizers.RMSprop(lr=1e-4),
              metrics=['accuracy'])
              
history = model.fit_generator(train_generator, steps_per_epoch=100, epochs=60,
                              validation_data=val_generator, validation_steps=50, 
                              verbose=1)   

model.save('baseA1B+.h5')

logFilePath = './logA1B+.txt'
fobj = open(logFilePath, 'a')
fobj.write('training accuracy: ' + str(history.history['accuracy'][-1]) + '\n')
fobj.close()

# f, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 4))
# t = f.suptitle('Basic CNN Performance', fontsize=12)
# f.subplots_adjust(top=0.85, wspace=0.3)

# epoch_list = list(range(1,31))
# ax1.plot(epoch_list, history.history['accuracy'], label='Train Accuracy')
# ax1.plot(epoch_list, history.history['val_accuracy'], label='Validation Accuracy')
# ax1.set_xticks(np.arange(0, 31, 5))
# ax1.set_ylabel('Accuracy Value')
# ax1.set_xlabel('Epoch')
# ax1.set_title('Accuracy')
# l1 = ax1.legend(loc="best")

# ax2.plot(epoch_list, history.history['loss'], label='Train Loss')
# ax2.plot(epoch_list, history.history['val_loss'], label='Validation Loss')
# ax2.set_xticks(np.arange(0, 31, 5))
# ax2.set_ylabel('Loss Value')
# ax2.set_xlabel('Epoch')
# ax2.set_title('Loss')
# l2 = ax2.legend(loc="best")
